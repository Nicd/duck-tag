[
  inputs: [
    "lib/**/*.{ex,exs}",
    "config/*.{ex,exs}",
    "mix.exs",
    "rel/config.exs"
  ]
]
