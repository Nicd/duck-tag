defmodule DuckTag.DB.Manager do
  @moduledoc """
  This module is responsible for defining and initialising the game state Mnesia database.
  """

  @typedoc """
  Game's status:

  - `:wait_connect` - Waiting for all nodes to connect.
  - `:init_players` - Initialising players.
  - `:init_respawns` - Initialising respawns.
  - `:game_on` - Game is running.
  - `:stopped` - Game was stopped or finished.
  """
  @type game_status ::
          :wait_connect | :init_players | :init_respawns | :wait_start | :game_on | :stopped

  @typedoc """
  Player's status in the game:

  - `:wait_start` - Waiting for the start of the game
  - `:alive` - Alive
  - `:dead` - Dead, can respawn at respawn point
  - `:out` - Out of the game, cannot respawn
  """
  @type player_status :: :wait_start | :alive | :dead | :out

  use GenServer
  use Amnesia
  require Logger

  defdatabase GameDB do
    deftable State, [{:id, autoincrement}, :status], type: :set do
      @type t :: %State{id: integer, status: DuckTag.DB.Manager.game_status()}

      def init_state() do
        State.write(%State{status: :wait_connect})
      end

      def set_status(%State{} = state, status) do
        State.write(%{state | status: status})
      end
    end

    deftable Player, [{:id, autoincrement}, :name, :status, :score, :last_respawn],
      type: :set,
      index: [:name] do
      @type t :: %Player{
              id: integer,
              name: String.t(),
              status: DuckTag.DB.Manager.player_status(),
              score: integer,
              last_respawn: DateTime.t() | nil
            }

      def add(name) do
        Player.write(%Player{name: name, status: :wait_start, score: 0, last_respawn: nil})
      end

      def init_player(%Player{} = player) do
        Player.write(%{player | score: 0, status: :alive})
      end

      def add_score(%Player{} = player, score) do
        Player.write(%{player | score: player.score + score})
      end

      def set_score(%Player{} = player, score) do
        Player.write(%{player | score: score})
      end

      def set_status(%Player{} = player, status) when is_atom(status) do
        Player.write(%{player | status: status})
      end

      def reset_last_respawn(%Player{} = player) do
        Player.write(%{player | last_respawn: DateTime.utc_now()})
      end
    end

    deftable Device, [:node, :rfid, :player_id, :type],
      type: :set,
      index: [:rfid, :player_id] do
      @type t :: %Device{
              node: atom,
              rfid: String.t(),
              player_id: integer,
              type: :player | :respawn
            }

      def add(node, tag_id, player_id, type) do
        if not Player.member?(player_id) do
          Logger.error("Unknown player #{player_id}!")
        else
          Device.write(%Device{node: node, rfid: tag_id, player_id: player_id, type: type})
        end
      end

      def set_rfid(%Device{} = device, tag_id) do
        Device.write(%{device | rfid: tag_id})
      end
    end
  end

  def start_link(opts) do
    GenServer.start_link(__MODULE__, nil, opts)
  end

  def init(_opts) do
    :ok = create_db()
    :ok = init_db()
    {:ok, :ok}
  end

  @doc """
  Create the database. This should only be run on node owning the DB.
  """
  def create_db() do
    Logger.debug("Starting Amnesia DB with mem copies on node #{inspect(node())}...")

    # XXX This sometimes says schema already exists?! Still seems to work though...
    Amnesia.Schema.create()

    # XXX WTF?!

    Logger.debug("Starting mnesia")
    start_db()

    Logger.debug("Stopping mnesia")
    stop_db()
    Process.sleep(1_000)

    Logger.debug("Starting mnesia again, because mnesia likes to be used this way")
    start_db()

    # Now after this mystical dance we can actually create the database
    GameDB.create!(memory: [node()])
    :ok = GameDB.wait(15_000)
  end

  @doc """
  Init game state to wait for nodes.
  """
  def init_db() do
    Amnesia.transaction do
      GameDB.State.init_state()
    end

    :ok
  end

  @doc """
  Start mnesia.
  """
  def start_db() do
    Amnesia.start()
  end

  @doc """
  Stop mnesia.
  """
  def stop_db() do
    Amnesia.stop()
  end

  @doc """
  Add node, copying all tables to it.
  """
  @spec add_node(atom) :: any
  def add_node(newnode) do
    Logger.debug("Received connection from remote node #{inspect(newnode)}!")

    # Ensure remote mnesia is stopped first, why? I do not know
    Logger.debug("Stopping mnesia on remote node")
    :rpc.call(newnode, Amnesia, :stop, [])

    copy_tables(newnode)

    Logger.debug("Starting mnesia on remote node")
    :rpc.call(newnode, Amnesia, :start, [])

    add_db_node(newnode)

    Logger.debug(
      "#{inspect(newnode)} is now connected to Mnesia Lightspeed Distributed Hyper Database"
    )
  end

  # Copy all tables to new node
  defp copy_tables(newnode) do
    Logger.debug("Adding Player copy to #{inspect(newnode)}")
    GameDB.Player.add_copy(newnode, :memory)
  end

  # Tell mnesia to add node as new node for table
  defp add_db_node(newnode) do
    Logger.debug("Adding #{inspect(newnode)} as mnesia extra DB node")
    :mnesia.change_config(:extra_db_nodes, [newnode])
  end
end
