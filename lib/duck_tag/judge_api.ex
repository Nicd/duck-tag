defmodule DuckTag.JudgeAPI do
  @moduledoc """
  Functions for talking to the Judge and a GenServer that manages the connection.
  """

  # How often to retry connecting to Judge, milliseconds
  @connect_retry_delay 2000

  use GenServer

  require Logger

  def start_link(opts) do
    GenServer.start_link(__MODULE__, nil, opts)
  end

  def init(_opts) do
    {:ok, try_connect()}
  end

  def handle_info(:retry_connect, {:connected, node}), do: {:noreply, {:connected, node}}
  def handle_info(:retry_connect, _state), do: {:noreply, try_connect()}

  defp try_connect() do
    remote_node = DuckTag.Utils.judge_node()

    case connect(remote_node) do
      true ->
        on_connect(remote_node)
        {:connected, remote_node}

      _ ->
        Process.send_after(self(), :retry_connect, @connect_retry_delay)
        :retrying
    end
  end

  defp connect(node) do
    Logger.debug("Attempting to connect to #{inspect(node)}...")
    Node.connect(node)
  end

  # Stuff to run when connection is established
  defp on_connect(node) do
    Logger.debug("Connected to #{inspect(node)}!")

    Logger.debug("Attempting to connect to Judge DB.")
    :rpc.call(node, DuckTag.DB.Manager, :add_node, [node()])
  end
end
