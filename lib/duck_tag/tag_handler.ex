defmodule DuckTag.TagHandler do
  @moduledoc """
  Tagging handler for devices.
  """
  alias DuckTag.DB.Manager.GameDB
  use Amnesia
  require Logger
  require GameDB.Device
  import Ex2ms

  @points_per_tag 100

  def tagged(tag_id) do
    {gs, od} =
      Amnesia.transaction do
        game_state = GameDB.State.first!()

        IO.inspect(game_state)

        our_device = GameDB.Device.read(node())

        {game_state, our_device}
      end

    tagged(od, gs, tag_id)
  end

  def tagged(nil, %GameDB.State{status: :init_players}, tag_id) do
    name =
      Amnesia.transaction do
        Logger.debug("Tagged by #{tag_id} so we are now a player.")

        name = UUID.uuid4()

        Logger.debug("Our name shall be #{name}!")

        GameDB.Player.add(name)
        name
      end

    Amnesia.transaction do
      id = get_player_id(name)
      Logger.debug("Our ID is #{id}.")

      Logger.debug("We have no device so let us create one.")
      GameDB.Device.add(node(), tag_id, id, :player)
    end
  end

  def tagged(%GameDB.Device{} = our_device, %GameDB.State{status: :init_players}, tag_id) do
    Amnesia.transaction do
      Logger.debug("Resetting device #{our_device.node} to tag #{tag_id}.")
      GameDB.Device.set_rfid(our_device, tag_id)
    end
  end

  def tagged(nil, %GameDB.State{status: :init_respawns}, tag_id) do
    Amnesia.transaction do
      Logger.debug("Tagged by #{tag_id} so we are now a respawn point.")

      GameDB.Device.add(node(), tag_id, nil, :respawn)
    end
  end

  def tagged(_, %GameDB.State{status: :init_respawns}, _) do
    Logger.error(
      "Cannot reassign spawn point. Please reinit game if you want to do that, ya dingus."
    )
  end

  def tagged(%GameDB.Device{type: :player} = our_device, %GameDB.State{status: :game_on}, tag_id) do
    our_player_id = our_device.player_id

    Amnesia.transaction do
      case GameDB.Device.read(tag_id) do
        nil ->
          Logger.debug("I was tagged by unknown tag #{tag_id}")

        %GameDB.Device{} = device ->
          case GameDB.Player.read(device.player_id) do
            nil ->
              Logger.debug("Tag #{tag_id} associated with nonexistent player #{device.player_id}")

            %GameDB.Player{id: player_id} when player_id == our_player_id ->
              Logger.debug("Cannot tag myself.")

            %GameDB.Player{status: status} = player when status != :alive ->
              Logger.debug("Tagged by #{player.name} but they are not in contention.")

            %GameDB.Player{status: :alive} = player ->
              our_player = GameDB.Player.read!(our_device.player_id)

              case our_player.status do
                :alive ->
                  Logger.debug("Tagged by #{player.name}. #{@points_per_tag} points!")
                  GameDB.Player.add_score(player, @points_per_tag)

                  # TODO: KILL OUR PLAYER
                  GameDB.Player.set_status(our_player, :dead)

                other ->
                  Logger.debug("Cannot be tagged, our status is #{other}.")
              end
          end
      end
    end
  end

  def tagged(%GameDB.Device{type: :respawn}, %GameDB.State{status: :game_on}, tag_id) do
    Amnesia.transaction do
      case GameDB.Device.read(tag_id) do
        nil ->
          Logger.debug("I was tagged by unknown tag #{tag_id}")

        %GameDB.Device{} = device ->
          case GameDB.Player.read(device.player_id) do
            nil ->
              Logger.debug("Tag #{tag_id} associated with nonexistent player #{device.player_id}")

            %GameDB.Player{} = player ->
              Logger.debug("I was tagged by #{player.name}. Respawned!")
              GameDB.Player.set_status(player, :alive)
              GameDB.Player.reset_last_respawn(player)
          end
      end
    end
  end

  def get_player_id(player_name) do
    GameDB.Player.select(
      fun do
        {_, id, name, _, _, _} when name == ^player_name -> id
      end
    )
    |> Amnesia.Selection.values()
    |> Enum.at(0, nil)
  end

  def get_our_device(tag_id) do
    selection =
      GameDB.Device.select(
        fun do
          {mod, node, rfid, player_id, type} when rfid == ^tag_id ->
            {mod, node, rfid, player_id, type}
        end
      )

    case Amnesia.Selection.coerce(selection, GameDB.Device)
         |> Amnesia.Selection.values()
         |> Enum.at(0, nil) do
      nil ->
        nil

      device ->
        device
    end
  end
end
