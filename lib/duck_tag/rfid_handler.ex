defmodule DuckTag.RFIDHandler do
  @moduledoc """
  Handles incoming RFID reads, delegating them to the appropriate handler code.
  """

  def tag_scanned(uid) do
    IO.puts("TAG SCANNED")
    IO.inspect(uid)

    DuckTag.TagHandler.tagged(uid)
  end
end
