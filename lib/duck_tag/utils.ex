defmodule DuckTag.Utils do
  @moduledoc """
  Util module for stuff that doesn't fit anywhere else.
  """

  @type role_t :: :common | :device | :judge

  @doc """
  Get project config.
  """
  @spec get_conf(atom) :: any
  def get_conf(key) do
    Application.get_env(:duck_tag, key)
  end

  @doc """
  Get the currently active role, set at build time.
  """
  @spec role() :: role_t
  def role() do
    get_conf(:role)
  end

  @doc """
  Get the Judge's node name.
  """
  @spec judge_node() :: atom
  def judge_node() do
    # Assume Judge has static IP
    "duck_tag_judge@#{DuckTag.Utils.get_conf(:judge_ip)}" |> String.to_atom()
  end
end
