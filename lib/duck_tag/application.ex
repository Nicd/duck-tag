defmodule DuckTag.Application do
  use Application
  import Supervisor.Spec, warn: false
  require Logger

  @doc """
  Get the workers that should be started for the given role.
  """
  @spec workers(DuckTag.Utils.role_t()) :: [any]
  def workers(type)

  def workers(:common) do
    []
  end

  def workers(:device) do
    [
      DuckTag.JudgeAPI.child_spec(name: DuckTag.Player.JudgeAPI),
      rfid_worker()
    ]
  end

  def workers(:judge) do
    [
      # Judge needs to start epmd manually or starting the node won't work
      DuckTag.NetUtils.EPMdStarter.child_spec(name: DuckTag.NetUtils.EPMdStarter),
      DuckTag.NetUtils.JudgeNameSetter.child_spec(name: DuckTag.NetUtils.JudgeNameSetter),
      DuckTag.DB.Manager.child_spec(name: DuckTag.DB.Manager)
    ]
  end

  def workers(unknown_role) do
    raise "Unknown role #{inspect(unknown_role)}!"
  end

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    device_type = DuckTag.Utils.role()

    # Define workers and child supervisors to be supervised
    children = Enum.concat(workers(:common), workers(device_type))

    Logger.debug("DuckTag.Supervisor starting up children:\n#{inspect(children)}")

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: DuckTag.Supervisor]
    Supervisor.start_link(children, opts)
  end

  defp rfid_worker(), do: worker(Nerves.IO.RC522, [{DuckTag.RFIDHandler, :tag_scanned}])
end
