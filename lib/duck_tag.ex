defmodule DuckTag do
  require Logger
  use Amnesia
  alias DuckTag.DB.Manager.GameDB
  import Ex2ms

  @doc """
  Start new game by adding new players. Do this when all nodes are connected.
  """
  def add_players() do
    Logger.debug("PLEASE ADD PLAYERS BY TAGGING DEVICES WITH THEIR RESPECTIVE TAGS.")

    GameDB.Device.clear()
    GameDB.Player.clear()

    Amnesia.transaction do
      GameDB.State.first!()
      |> GameDB.State.set_status(:init_players)
    end
  end

  @doc """
  Add respawn points, do this after adding players.
  """
  def add_respawns() do
    Logger.debug("PLEASE ADD RESPAWN POINTS BY TAGGING DEVICES WITH ANY TAG.")

    Amnesia.transaction do
      GameDB.State.first!()
      |> GameDB.State.set_status(:init_respawns)
    end
  end

  @doc """
  Start gameplay!
  """
  def start_game() do
    Logger.debug("Resetting players...")

    Amnesia.transaction do
      selection =
        GameDB.Player.select(
          fun do
            p -> p
          end
        )

      selection
      |> Amnesia.Selection.coerce(GameDB.Player)
      |> Amnesia.Selection.values()
      |> Enum.each(fn player ->
        GameDB.Player.init_player(player)
      end)
    end

    Amnesia.transaction do
      GameDB.State.first!()
      |> GameDB.State.set_status(:game_on)
    end
  end

  @doc """
  Stop running game.
  """
  def stop_game() do
    Amnesia.transaction do
      selection =
        GameDB.Player.select(
          fun do
            p -> p
          end
        )

      selection
      |> Amnesia.Selection.coerce(GameDB.Player)
      |> Amnesia.Selection.values()
      |> Enum.each(&GameDB.Player.set_status(&1, :out))
    end

    Amnesia.transaction do
      GameDB.State.first!()
      |> GameDB.State.set_status(:stopped)
    end
  end
end
