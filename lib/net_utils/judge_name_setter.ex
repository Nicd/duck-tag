defmodule DuckTag.NetUtils.JudgeNameSetter do
  use GenServer

  alias DuckTag.NetUtils.NodeStart

  require Logger

  def start_link(opts) do
    GenServer.start_link(__MODULE__, nil, opts)
  end

  def init(_opts) do
    Logger.debug("JudgeNameSetter starting node")
    {:ok, pid} = NodeStart.start()
    {:ok, pid}
  end
end
