defmodule DuckTag.NetUtils.EPMdStarter do
  @moduledoc """
  Starts and manages EPMd on nodes that need it started manually (static IP).
  """

  use GenServer

  require Logger

  def start_link(opts) do
    GenServer.start_link(__MODULE__, nil, opts)
  end

  def init(_opts) do
    Logger.debug("Starting EPMd manually...")
    :os.cmd('epmd -daemon')

    {:ok, nil}
  end
end
