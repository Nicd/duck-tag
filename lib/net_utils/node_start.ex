defmodule DuckTag.NetUtils.NodeStart do
  @moduledoc """
  Utils related to setting up node for nodes with static IP.
  """

  require Logger

  @doc """
  Start node with name appropriate for current role.
  """
  def start() do
    start(:judge)
  end

  @doc """
  Start a role specific node.
  """
  @spec start(DuckTag.role()) :: {:ok, pid} | {:error, term}
  def start(role)

  def start(:judge) do
    Logger.debug("Setting node name to '#{inspect(DuckTag.Utils.judge_node())}'")

    Node.start(DuckTag.Utils.judge_node())
  end
end
