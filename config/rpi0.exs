# RPI0W specific configs

use Mix.Config

config :nerves_init_net_kernel,
  iface: "wlan0"

import_config "rpi0.secret.exs"
