# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Customize the firmware. Uncomment all or parts of the following
# to add files to the root filesystem or modify the firmware
# archive.

# config :nerves, :firmware,
#   rootfs_overlay: "rootfs_overlay",
#   fwup_conf: "config/fwup.conf"

# Use bootloader to start the main application. See the bootloader
# docs for separating out critical OTP applications such as those
# involved with firmware updates.
config :bootloader,
  init: [:nerves_runtime, :nerves_init_gadget, :nerves_network, :nerves_init_net_kernel],
  app: :duck_tag

config :nerves_firmware_ssh,
  authorized_keys: [
    File.read!(Path.join(['ssh', 'id_nerves.pub']))
  ]

# Autoconnect to WiFi set at build time with env vars
key_mgmt = System.get_env("NERVES_NETWORK_KEY_MGMT") || "WPA-PSK"

# By default all nodes will try to connect to given WiFi or wired ethernet with DHCP
config :nerves_network, :default,
  wlan0: [
    ssid: System.get_env("NERVES_NETWORK_SSID"),
    psk: System.get_env("NERVES_NETWORK_PSK"),
    key_mgmt: String.to_atom(key_mgmt)
  ],
  eth0: [
    ipv4_address_method: :dhcp
  ]

# Override this in further config
config :nerves_network, regulatory_domain: "00"

# Overrides for networking that apply to every device
import_config "networking.secret.exs"

# Import target specific config

import_config "#{Mix.Project.config()[:target]}.exs"

# Include role specific config as last. There you can set custom configs based on the role of the device
if System.get_env("DUCKTAG_ROLE") do
  config :duck_tag, role: "DUCKTAG_ROLE" |> System.get_env() |> String.to_atom()

  import_config "#{System.get_env("DUCKTAG_ROLE")}.exs"
end
