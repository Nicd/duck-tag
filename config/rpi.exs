# RPi 1 Model B specific config

use Mix.Config

config :nerves_init_net_kernel,
  iface: "eth0"

import_config "rpi.secret.exs"
